package com.lab8.lab8.repository;

import com.lab8.lab8.model.Disciplina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

@RepositoryRestResource
public interface DisciplinaRepository extends JpaRepository<Disciplina, Long> {

    @Secured("MANAGER")
    @RestResource(path = "/datainicio")
    @Query("SELECT disciplina FROM Disciplina disciplina WHERE disciplina.dataInicio > CURRENT_DATE")
    public List<Disciplina> findByDataInicioAfterNow();
}
